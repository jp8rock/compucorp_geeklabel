<section class="s-common s-hero">
    <article class="s-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-5 col-lg-center">
                    <div class="logo text-center">
                        <?php print '<img class="img-responsive center-block" src="'. drupal_get_path('theme','geeklabeltheme') . '/images/logo.png' .'" alt="geek label" />'?>
                    </div>
                    <p class="text-center fs-18 fw-300">A team of self confessed geek who are all about great digital design</p>
                </div>
            </div>
        </div>
    </article>
    <div class="next-section text-center">
        <a href="" class="fw-300 btn-next">
           <span class="btn-circle">
               <span class="lnr lnr-chevron-down fw-bold fs-16"></span>
           </span>
        </a>
    </div>
</section>

<section class="s-common s-1">
    <article class="s-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 mb30">
                    <div class="logo text-center">
                        <?php print '<img class="img-responsive center-block" src="'. drupal_get_path('theme','geeklabeltheme') . '/images/s1.png' .'" alt="geek label" />'?>
                    </div>

                </div>
            </div>
        </div>
    </article>
    <div class="next-section text-center">
        <p class="text-center fs-18">Some agencies love <span class="text-primary">design</span>, but don’t know how to build</p>
        <a href="" class="primary fw-300 btn-next">
           <span class="btn-circle">
               <span class="lnr lnr-chevron-down fw-bold fs-16"></span>
           </span>
        </a>
    </div>
</section>

<section class="s-common s-1">
    <article class="s-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 mb30">
                    <div class="logo text-center">
                        <?php print '<img class="img-responsive center-block" src="'. drupal_get_path('theme','geeklabeltheme') . '/images/s2.png' .'" alt="geek label" />'?>
                    </div>

                </div>
            </div>
        </div>
    </article>
    <div class="next-section text-center">
        <p class="text-center fs-18">Some agencies know how to <span class="text-primary">build</span> but can’t do design</p>
        <a href="" class="primary fw-300 btn-next">
           <span class="btn-circle">
               <span class="lnr lnr-chevron-down fw-bold fs-16"></span>
           </span>
        </a>
    </div>
</section>

<section class="s-common s-1">
    <article class="s-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 mb30">
                    <div class="logo text-center">
                        <?php print '<img class="img-responsive center-block" src="'. drupal_get_path('theme','geeklabeltheme') . '/images/s3.png' .'" alt="geek label" />'?>
                    </div>
                </div>
            </div>
        </div>
    </article>
    <div class="next-section text-center">
        <p class="text-center fs-18">We love <span class="text-primary">both</span></p>
        <a href="" class="primary fw-300 btn-next">
           <span class="btn-circle">
               <span class="lnr lnr-chevron-down fw-bold fs-16"></span>
           </span>
        </a>
    </div>
</section>

<section class="s-common s-services">

    <div class="container">
        <div class="row">
            <div class="col-sm-12 mb30">
                <h1 class="text-center">Services</h1>
            </div>
        </div>
    </div>

    <article class="s-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="row s-xs-mb">
                        <div class="col-xs-3 col-sm-12">
                            <div class="s-services-icon-bg">
                                <span>
                                    <?php print '<img class="img-responsive center-block" src="'. drupal_get_path('theme','geeklabeltheme') . '/images/a1.png' .'" alt="geek label" />'?>
                                </span>
                            </div>
                        </div>
                        <div class="col-xs-9 col-sm-12">
                            <h3>We Development</h3>
                        </div>
                    </div>

                </div>
                <div class="col-sm-3">
                    <div class="row s-xs-mb">
                        <div class="col-xs-3 col-sm-12">
                            <div class="s-services-icon-bg">
                                <span>
                                    <?php print '<img class="img-responsive center-block" src="'. drupal_get_path('theme','geeklabeltheme') . '/images/a2.png' .'" alt="geek label" />'?>
                                </span>
                            </div>
                        </div>
                        <div class="col-xs-9 col-sm-12">
                            <h3>Design</h3>
                        </div>
                    </div>

                </div>
                <div class="col-sm-3">
                    <div class="row s-xs-mb">
                        <div class="col-xs-3 col-sm-12">
                            <div class="s-services-icon-bg">
                                <span>
                                <?php print '<img class="img-responsive center-block" src="'. drupal_get_path('theme','geeklabeltheme') . '/images/a3.png' .'" alt="geek label" />'?>
                                </span>
                            </div>
                        </div>
                        <div class="col-xs-9 col-sm-12">
                            <h3>Branding</h3>
                        </div>
                    </div>

                </div>
                <div class="col-sm-3">
                    <div class="row s-xs-mb">
                        <div class="col-xs-3 col-sm-12">
                            <div class="s-services-icon-bg">
                                <span>
                                    <?php print '<img class="img-responsive center-block" src="'. drupal_get_path('theme','geeklabeltheme') . '/images/a4.png' .'" alt="geek label" />'?>
                                </span>
                            </div>
                        </div>
                        <div class="col-xs-9 col-sm-12">
                            <h3>UX Research</h3>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </article>
    <div class="next-section text-center">
        <a href="" class="dark fw-300 btn-next">
           <span class="btn-circle">
               <span class="lnr lnr-chevron-down fw-bold fs-14"></span>
           </span>
        </a>
    </div>
</section>


<section class="s-common s-clients">

<div class="container">
    <div class="row">
        <div class="col-sm-12 mb30">
            <h1 class="text-center">Clients</h1>
        </div>
    </div>
</div>



            <article class="s-center">
                <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="b-slider">
                        <input type="radio" id="b-slide1" name="b-slide" checked>
                        <input type="radio" id="b-slide2" name="b-slide">
                        <input type="radio" id="b-slide3" name="b-slide">

                        <input type="radio" id="b-slide4" name="b-slide">
                        <input type="radio" id="b-slide5" name="b-slide">
                        <input type="radio" id="b-slide6" name="b-slide">
                        <input type="radio" id="b-slide7" name="b-slide">



                        <div class="b-slides">
                            <div class="overflow">
                                <div class="inner row">
                                    <article class="col-sm-4">
                                        <a href="#">
                                            <?php print '<img src="' . drupal_get_path('theme','geeklabeltheme') . '/images/c1.png' . '" alt="cancer research uk" />' ?>
                                        </a>
                                    </article>
                                    <article class="col-sm-4">
                                        <a href="#">
                                            <?php print '<img src="' . drupal_get_path('theme','geeklabeltheme') . '/images/c2.png' . '" alt="cancer research uk" />' ?>
                                        </a>
                                    </article>
                                    <article class="col-sm-4">
                                        <a href="#">
                                            <?php print '<img src="' . drupal_get_path('theme','geeklabeltheme') . '/images/c3.png' . '" alt="cancer research uk" />' ?>
                                        </a>
                                    </article>
                                    <article class="col-sm-4">
                                        <a href="#">
                                            <?php print '<img src="' . drupal_get_path('theme','geeklabeltheme') . '/images/c1.png' . '" alt="cancer research uk" />' ?>
                                        </a>
                                    </article>
                                    <article class="col-sm-4">
                                        <a href="#">
                                            <?php print '<img src="' . drupal_get_path('theme','geeklabeltheme') . '/images/c2.png' . '" alt="cancer research uk" />' ?>
                                        </a>
                                    </article>
                                    <article class="col-sm-4">
                                        <a href="#">
                                            <?php print '<img src="' . drupal_get_path('theme','geeklabeltheme') . '/images/c3.png' . '" alt="cancer research uk" />' ?>
                                        </a>
                                    </article>
                                    <article class="col-sm-4">
                                        <a href="#">
                                            <?php print '<img src="' . drupal_get_path('theme','geeklabeltheme') . '/images/c1.png' . '" alt="cancer research uk" />' ?>
                                        </a>
                                    </article>

                                </div>
                            </div>
                        </div>
                        <div class="b-slide-control mt20 hidden-xs">
                            <label for="b-slide1"></label>
                            <label for="b-slide2"></label>
                            <label for="b-slide3"></label>
                        </div>

                        <div class="b-slide-indicator mt20 visible-xs">
                            <label for="b-slide1"></label>
                            <label for="b-slide2"></label>
                            <label for="b-slide3"></label>
                            <label for="b-slide4"></label>
                            <label for="b-slide5"></label>
                            <label for="b-slide6"></label>
                            <label for="b-slide7"></label>
                        </div>
                    </div>
                </div>
            </div>
                </div>
                </article>


    <div class="next-section text-center">
        <a href="" class="dark fw-300 btn-next">
           <span class="btn-circle">
               <span class="lnr lnr-chevron-down fw-bold fs-16"></span>
           </span>
        </a>
    </div>
</section>

<section class="s-common s-map">
    <article class="">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 mb30">
                    <h1 class="text-center hidden-xs">How to find us</h1>
                    <h1 class="text-center visible-xs">Find us</h1>
                </div>
            </div>
        </div>
    </article>

    <article class="s-center">

        <div class="row">
            <div class="col-md-12 p0">
                <div class="map">

                </div>
            </div>
        </div>

    </article>
    <div class="next-section text-center">
        <a href="" class="primary fw-300 btn-next">
           <span class="btn-circle">
               <span class="lnr lnr-chevron-down fw-bold fs-16"></span>
           </span>
        </a>
    </div>
</section>

<section class="s-common s-contact">

    <div class="container">
        <div class="row">
            <div class="col-sm-12 mb30">
                <h1 class="text-center">Contact</h1>
            </div>
        </div>
    </div>

    <article class="s-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-5 col-lg-center">
                    <form action="">
                        <div class="form-group">
                            <label for="name" class="sr-only">Name:</label>
                            <input type="text" class="form-control" placeholder="Name" id="name">
                        </div>
                        <div class="form-group">
                            <label for="email" class="sr-only">Email:</label>
                            <input type="email" class="form-control" placeholder="Email" id="email">
                        </div>
                        <div class="form-group">
                            <label for="message" class="sr-only">Message:</label>
                            <textarea  class="form-control" placeholder="Message" id="message" rows="10"></textarea>
                        </div>
                        <button type="submit" class="btn btn-block btn-primary btn-submit">Send Message!</button>
                    </form>
                    <p class="text-center hidden-xs">Or phone on: 01923 220121</p>
                    <p class="visible-xs text-center s-c-phone fs-18 "><i class="lnr lnr-phone-handset"></i> 01923 220121</p>
                    <p class="visible-xs text-center s-c-email fs-18"><i class="lnr lnr-envelope"></i> info@compucorp.co.uk</p>
                </div>
            </div>
        </div>
    </article>


</section>